package com.solomia;

import com.solomia.listener.TestAllureListener;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Listeners;

@Listeners({TestAllureListener.class})
public class Test {
    private static Logger logger = LogManager.getLogger(Test.class);


    @org.testng.annotations.Test
    public void first(){
        String name = "Solomia";
        logger.info("Name : " + name);
        fi(name);
        rst();
        Assert.assertTrue(name.contains("Nazar"));
    }

    @Step(value = "Input name {0}")
    public void fi(String name) {
        System.out.print(name);
    }

    @Step
    public void rst(){
        System.out.println("ggfy");
    }


}
