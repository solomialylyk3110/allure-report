package com.solomia.decorator;

import com.solomia.factory.Wait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Button extends AbstractElement {
    public Button(WebElement webElement) {
        super(webElement);
    }

    public void click() {
        webElement.click();
    }
    
    public void waitUntilVisibleAndClick() throws InterruptedException {
        Wait.until(ExpectedConditions.visibilityOf(webElement)).click();
    }
}
